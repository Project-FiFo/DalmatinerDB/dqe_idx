-module(dqe_idx_none).
-behaviour(dqe_idx).

%% API exports
-export([init/0,
         lookup/4, lookup/5, lookup_tags/1,
         collections/0, metrics/1, metrics/2, metrics/3,
         namespaces/1, namespaces/2,
         tags/2, tags/3, values/3, values/4, expand/2,
         add/5, add/6, update/5, touch/1,
         delete/4, delete/5]).

%%====================================================================
%% API functions
%%====================================================================


init() ->
    %% We do not need to initialize anything here.
    ok.

lookup(_Q, _Start, _Finish, _G, _Opts) ->
    {ok, []}.

lookup(_Q, _Start, _Finish, _Opts) ->
    {ok, []}.

lookup_tags(_) ->
    {ok, []}.

expand(Bkt, Globs) ->
    {ok, {Bkt, Globs}}.

metrics(_Bucket, _Tags) ->
    {ok, []}.

metrics(_Collection, _Prefix, _Depth) ->
    {ok, []}.

collections() ->
    {ok, []}.

metrics(_) ->
    {ok, []}.

namespaces(_) ->
    {ok, []}.

namespaces(_, _) ->
    {ok, []}.

tags(_, _) ->
    {ok, []}.

tags(_, _, _) ->
    {ok, []}.

values(_, _, _) ->
    {ok, []}.

values(_, _, _, _) ->
    {ok, []}.

touch(_) ->
    ok.

add(_, _, _, _, _) ->
    {ok, 0}.

add(_, _, _, _, _, _) ->
    {ok, 0}.

update(_, _, _, _, _) ->
    {ok, 0}.

delete(_, _, _, _) ->
    ok.

delete(_, _, _, _, _) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
